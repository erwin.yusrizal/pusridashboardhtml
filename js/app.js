(function($){

    $.pusrialert = function(options){

        var defaults = {
            title: 'Warning',
            message: 'Your message here...',
            btnCloseText: 'Close',
            backdrop: true,
            onClosed: null
        };

        var o = $.extend({}, defaults, options);

        var closeButton = $('<button/>', {class: 'pusrialert-close bg-blue color-white', text: o.btnCloseText});
            alertHeaderEl = $('<div/>', {class: 'pusrialert-header'}).html(o.title),
            alertMessageEl = $('<div/>', {class: 'pusrialert-message'}).html(o.message),
            alertFooterEl = $('<div/>', {class: 'pusrialert-footer'}).append(closeButton);

        var alertEl = $('<div/>', {class: 'pusrialert'}).append([alertHeaderEl, alertMessageEl, alertFooterEl]),
            backdropEl = $('<div/>', {class: 'pusrialert-backdrop'});            

        if(o.backdrop == true){
            backdropEl.append(alertEl).appendTo('body').hide().fadeIn();
        }else{
            alertEl.appendTo('body').hide().fadeIn();
        }

        closeButton.on('click', function(e){
            if(o.backdrop){
                backdropEl.fadeOut(function(){
                    this.remove();
                    if(typeof o.onClosed === "function"){
                        return o.onClosed(e);
                    }
                });
            }else{
                alertEl.fadeOut(function(){
                    this.remove();
                    if(typeof o.onClosed === "function"){
                        return o.onClosed(e);
                    }
                });
            }
        });
    };

})(jQuery);


(function($, window, document, undefined) {

    var app = app || {};

    app.login = function(){
        $(document).on('click', '#login button', function(e){
            $.pusrialert({
                title: '<strong>Warning</strong>',
                message: 'Your message here...',
                btnCloseText: 'Tutup',
                backdrop: false,
                onClosed: function(){
                    console.log('closed!');
                }
            });
        });
    };

    app.init = function(){
        this.login();
    };

    $(document).ready(function(){
        app.init();
    });

}(jQuery, window, document));