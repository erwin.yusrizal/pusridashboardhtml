# PUSRI DASHBORD
Please do all the task below. Use html, css, and javascript. For DOM manipulation, you can use jQuery which is already included.

### LOGIN PAGE
By default login page will show the login credential input, here we use mobile phone number to as user login credential.

1. Create a validation for mobile phone number entered, valid phone number for both using 08 or +62 or 62 format.
2. You can use your own phone number as a valid phone number
3. if valid, show the verification page, once click **verifikasi** button, go to dashboard.html


### DASHBOARD PAGE
Dashboard has some components, e.g: sidebar, navbar, history bar and main content. 
1. Make the sidebar able to hide and show. When you close the sidebar, it will show only the icon
2. Create a sub nav for each sidebar menu, also add default arrow icon for expand and collapsed sidebar menu
3. In top varbar right menu: notification, histories and  profile, make it clickable and show a dropdown menu for each menu items
